import axios from 'axios'
import router from '../router'
axios.defaults.baseURL = 'https://api-demo.websanova.com/api/v1'
export default {
  pesquisarDia ({ commit }, payload) {
    commit('setLoading', true)
  },
  // cad de usuario
  singUserUp ({ commit }, payload) {
    commit('setLoading', true)
    commit('clearError')
    const url = 'http://localhost:5000/user'
    axios.post(url, payload)
      .then((res) => {
        if (res.data.msg === 'Usuario cadastrado com sucesso') {
          router.replace('/')
          commit('setLoading', false)
        } else {
          commit('setError', res.data.msg)
        }
        if (res.data.erro) commit('setError', res.data.erro)
      })
  },
  singUserIn ({ commit }, payload) {
    commit('setLoading', true)
    commit('clearError')
    const url = 'http://localhost:5000/login'
    axios.post(url, payload)
      .then((res) => {
        if (res.data.msg === 'usuario autenticado') {
          router.replace('/cad/servico')
          commit('setLoading', false)
        }
      })
      .catch(err => {
        const erro = {message: err.response.data}
        commit('setError', erro)
        commit('setLoading', false)
      })
    // login de usuario
  },
  autoSignIn ({ commit }, payload) {
    commit('setUser', { id: payload.uid, registeredMeetups: [] })
  },
  clearError ({ commit }) {
    commit('clearError')
  }
}
