import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Profile from '@/components/User/Profile'
import Singin from '@/components/User/Singin'
import Singup from '@/components/User/Singup'
import AuthGuard from './auth-guard'
import listaTosa from '@/components/dash/lista_tosa'
import cadServ from '@/components/dash/cadServico'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/cad/servico',
      name: 'CadServico',
      component: cadServ
    },
    {
      path: '/lista/tosa',
      name: 'ListaTosa',
      component: listaTosa
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/singup',
      name: 'Singup',
      component: Singup
    },
    {
      path: '/singin',
      name: 'Singin',
      component: Singin
    }
  ],
  mode: 'history'
})
