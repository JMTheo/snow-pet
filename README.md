# Snow Pet
> An admin template for petshop commerce

## Build Setup

``` bash
# install dependencies
npm install or yarn

# serve with hot reload at localhost:8080
npm run dev or yarn dev

# build for prod
npm run build or yarn build